# Setting Up Spack

We use a Spack 'sandbox' to test installations and
make sure that the dependencies pulled are sane, and all errors
in installation have been resolved prior to installing into the
production environment.

Spack is set up mostly the same way regardless whether it is the
sandbox or a new production version (as on a new cluster).  Some
configuration files will be different to account for different
paths, but that should be the only difference.

As of May 2024, we are initiating a "new" configuration to install
packages in paths that are parallel to those for manually installed
packages.  This repo, `vaccspack`, along with the configurations
contained within, is to document the setup and use of a new spack
instance for general use by VACC and for installation of packages, as
requested by users.  The root of this spack instance is at
`/gpfs1/sw/spack/<ver>`. The planned structure under that is:

```
/gpfs1/sw/spack/<ver>
    /modules          # Spack installed module files
    /local-packages   # locally modified Spack packages
    /pkgs             # Spack installed software
    /*                # Spack itself
```

We recommend changing only the root and keeping a parallel
structure underneath.  In this example, we will use
`$HOME/spk-sandbox` as the root for the sandbox and
the path above as the production path.  NOTE:  Spack also
has a `$user` variable; we will use the all uppercase version
to mean the shell variable and the all lowercase version to
mean the Spack variable.

# Spack configuration

The first configuration step is to set

```
export SPACK_USER_CACHE_PATH=$HOME/scratch/spack-cache
export SPACK_DISABLE_LOCAL_CONFIG=true
```

Add the above lines to your `$HOME/.bashrc` file, so that they are
automatically set each time you log in.

The value of `SPACK_USER_CACHE_PATH` becomes the value of the
Spack variable `user_cache_path`, which is used in Spack
configuration files.  The `SPACK_DISABLE_LOCAL_CONFIG`
prevents Spack from putting configuration information into
`~/.spack`, which might hide something from you or others.

Clone Spack source, where <ver> is the desired release version.

```
$ cd $HOME/spk-sandbox/spack
$ git clone -c feature.manyFiles=true https://github.com/spack/spack.git <ver>
```

Checkout the tag corresponding to the release you wish to
use; in this case, we are using 0.21.2 as our desired release.

```
$ cd 0.21.2
$ git checkout -b v0.21.2 v0.21.2
Switched to a new branch 'v0.21.2'
```

Create the directories needed.

```
$ for dir in local-packages pkgs modules ; do
    mkdir -p $HOME/spk-sandbox/spack/0.21.2/$dir
done
```

Edit the `.gitignore` file to exclude our directory additions.
Include the following text and save:

```
##########################
# VACC-specific ignores  #
##########################

modules/
local-packages/
pkgs/
```

Clone _this_ repository if you have not already.

```
$ cd $HOME/spk-sandbox
$ git clone git@gitlab:vacc-help/vaccspack.git
```

Copy `repo.yaml` from the `vaccspack` repo
to the `local-packages` directory.

```
$ cp vaccspack/local-packages/repo.yaml spack/0.21.2/local-packages/.
```

Next, if the`packages` directory exists, copy it to `local-packages`.

```
$ cp -R vaccspack/local-packages/packages spack/0.21.2/local-packages
```

If there is not a `packages` directory in `vaccspack/local-packages`
available to copy, create an empty `packages` directory in
`spack/0.21.2/local-packages`:

```
$ mkdir -p spack/0.21.2/local-packages/packages
```

Copy the configuration files from `vaccspack` and
modify for your configuration.

```
$ cd spack/0.21.2/etc/spack
$ cp $HOME/spk-sandbox/vaccspack/spack/etc/spack/*.yaml .
```

Modify `config.yaml` by choosing the correct `root:` attribute for
`install_tree:`.  Be sure to replace ${SPACK_VER} with the correct
version.  Version 0.21.2 is used in this example.

For example, `config.yaml` would look like this for a personal
sandbox.

```
config:
  # This is the path to the root of the Spack install tree.
  install_tree:
    # Use this for private
    root: $HOME/spk-sandbox/spack/0.21.2/pkgs
    projections:
      all: "{architecture}/{compiler.name}-{compiler.version}/{name}/{version}-{hash:7}"
  build_stage:
    - $HOME/scratch/spack-stage
  test_stage: $user_cache_path/test
```

Next, modify `modules.yaml` to choose the correct `lmod:` path under
`roots:`.  For example, the beginning of `modules.yaml` would look
like this for a personal sandbox.

```
modules:
  default:
     # Where to install modules
    roots:
      # Use this for private
      lmod: $HOME/spk-sandbox/spack/${SPACK_VER}/modules
    # arch_folder: false
    enable:
      - lmod
```

Where you should replace ${SPACK_VER} with the correct version.

Now modify `repos.yaml` to choose the correct repo for local package
modifications.  For a personal sandbox, it should look like this:

```
repos:
# Use this for private
- $HOME/spk-sandbox/spack/${SPACK_VER}/local-packages
- $spack/var/spack/repos/builtin
```
where ${SPACK_VER} should be replaced with the correct version.

Next, source the setup file.

```
$ source $HOME/spk-sandbox/spack/0.21.2/share/spack/setup-env.sh
```

We need to register the system compiler (gcc@4.8.5) next. The
following command will add the system compiler to the `compilers.yaml`
file in `spack/etc/spack/linux` as well as to the `spack.yaml`
file in any environments you have created (e.g. 'vacc`).

```
$ spack compiler add $(spack location -i gcc@4.8.5)
```

Now install a more recent version of `gcc` that will be used as the
default compiler for most future installations.  We have selected
version `13.2.0` for our current iteration (May 20, 2024).

```
$ spack install gcc@13.2.0
```

This will take quite a while. When that finishes, register the new compiler:

```
$ spack compiler add $(spack location -i gcc@13.2.0)
```

Uncomment the new `gcc` compiler in the `packages.yaml` file at
`spack/etc/spack/packages.yaml`.  The first few lines of the file
should now look like:

```
packages:
  all:
    compiler: [gcc@13.2.0]
    variants: ~mpi
```

You should now add any packages to the `packages.yaml` file that you
want to constrain to a particular version, or which you want to
specify to use the system version (`buildable: false`).  If there are
currently any packages listed (or commented out) that were there when
you copied `packages.yaml` from the repo, and you wish to use those as
constraints, the version numbers should be updated as appropriate
(e.g. matching the versions of any common dependencies that were
installed for the `gcc` compiler or matching the versions of non-spack
installed packages).  Bennet Fauber initiated the practice of setting
versions/variants for common dependencies when we were setting up the
first U of Michigan Spack instance.  This seemed to work fine, mostly,
but we did encounter some issues, particularly with having `openssl`
defined.  It was likely conflicting with the system installed version,
which was different.  For the VACC Spack instance, we are trying a
less constrained approach.
