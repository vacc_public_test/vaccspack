# Setting Up Spack

Rightly or not, we use a Spack 'sandbox' to test installations and
make sure that the dependencies pulled are sane, and all errors
in installation have been resolved prior to installing into the
production environment.

Spack is set up mostly the same way regardless whether it is the
sandbox or a new production version (as on a new cluster).  Some
configuration files will be different to account for different
paths, but that should be the only difference.

As of May 2024, we have a complete production environment to install
and manage a set of Spack packages.  The root of that installation is
at `/gpfs1/sw/spack/<ver>`.  This file documents it's setup and
configuration.

This repo, `vaccspack`, along with the configurations contained
within, is to document the setup and use of a new spack instance for
general use by VACC.  The structure under the VACC Spack root
(`/gpfs1/sw/spack/<ver>`) is as follows:

```
/gpfs1/sw/spack/<ver>
    /local-packages   # locally modified Spack packages
    /modules          # Spack installed module files
    /pkgs             # Spack installed software
    /*                # Spack itself
```

Herewithin, we document the steps for setup of the production
environment.  The root for the production installation is
`/gpfs1/sw/spack/<ver>`, while the underlying setup should mostly parallel
structure described for setting up a sandbox for testing.  The
production installation is configured and maintained by VACC staff
using the `spackusr` account.

# Spack configuration

The first configuration step is to set

```
SPACK_USER_CACHE_PATH=$HOME/scratch/spack-cache
SPACK_DISABLE_LOCAL_CONFIG=true
```

Add the above lines to the `$HOME/.bashrc` file of the `spackusr`
(Spack installer) account, so that they are automatically set each
time you log in as that user.

The value of `SPACK_USER_CACHE_PATH` becomes the value of the
Spack variable `user_cache_path`, which is used in Spack
configuration files.  The `SPACK_DISABLE_LOCAL_CONFIG`
prevents Spack from putting configuration information into
`~/.spack`, which might hide something from you or others.

Clone Spack source.

```
$ cd /gpfs1/sw/spack
$ git clone -c feature.manyFiles=true git@github.com:spack/spack.git <ver>
```

Checkout the tag corresponding to the release you wish to
use; in this case, we are using 0.21.2 as our desired release.

```
$ cd 0.21.2
$ git checkout -b v0.21.2 v0.21.2
Switched to a new branch 'v0.21.2'
```

Create the directories needed.

```
$ for dir in local-packages modules pkgs ; do
    mkdir -p /gpfs1/sw/spack/0.21.2/$dir
done
```

Edit the `.gitignore` file to exclude our directory additions.
Include the following text and save:

```
##########################
# VACC-specific ignores  #
##########################

modules/
local-packages/
pkgs/
```

Clone _this_ repository in your own workspace at `$HOME/spk-sandbox`
if you have not already.  That is where your spack sandbox should be
set up.  We will assume that `vaccspack` and your versioned Spack
sandbox are in the same directory hereafter.  Below describes the next
steps for installing and configuring spack for production..

```
$ cd $HOME/spk-sandbox
$ git clone git@gitlab.uvm.edu:vacc_help/vaccspack.git
```

Copy `repo.yaml` from the `vaccspack` repo to the production
`local-packages` directory.

```
$ cd /gpfs1/sw/spack/0.21.2
$ cp $HOME/spk-sandbox/vaccspack/local-packages/repo.yaml local-packages/.
```

Next, if the `packages directory exists, copy it to `local-packages`.
```
$ cp -R $HOME/spk-sandbox/vaccspack/local-packages/packages local-packages
```

If there is not a `packages` directory in `vaccspack/local-packages`
available to copy, create an empty `packages` directory in
`/gpfs1/sw/spack/0.21.2/local-packages`:

```
$ mkdir -p /gpfs1/sw/spack/0.21.2/local-packages/packages
```

Copy the configuration files from `vaccspack` to the production Spack
installation and modify for your configuration.

```
$ cd /gpfs1/sw/spack/0.21.2/spack/etc/spack
$ cp $HOME/spk-sandbox/vaccspack/spack/etc/spack/*.yaml .
```

Modify `config.yaml` by choosing the correct `root:` attribute for
`install_tree:`.  Be sure to replace ${SPACK_VER} with the correct
version.  Version 0.21.2 is used in this example.

For example, `config.yaml` should look like this for production:

```
config:
  # This is the path to the root of the Spack install tree.
  install_tree:
    # Use this for production
    root: /gpfs1/sw/spack/0.21.2/pkgs
    projections:
      all: "{architecture}/{compiler.name}-{compiler.version}/{name}/{version}-${hash:7}"
  build_stage:
    - $HOME/scratch/spack-stage
  test_stage: $user_cache_path/test
```

Next, modify `modules.yaml` to choose the correct `lmod:` path under
`roots:`.  Also, decide if you want to change the default hash length
(7).  The range is 0 to 32.  To change from the default, uncomment
`hash_length` and set as desired.  The shorter the hash length, the
more likely you will need to make use of the suffixes option to avoid
conflicts.  The beginning of `modules.yaml` should look like this for
production:

```
modules:
  default:
     # Where to install modules
    roots:
      # Use this for production
      lmod: /gpfs1/sw/spack/${SPACK_VER}/modules
    enable:
      - lmod
    lmod:
          hash_length: 7
          all:
```

Where you should replace ${SPACK_VER} with the correct version. Next,
source the setup file.

```
source /gpfs1/sw/spack/${SPACK_VER}/spack/share/spack/setup-env.sh
```

Again, you should replace ${SPACK_VER} with the correct version.

We need to register the system compiler (gcc@4.8.5) next. The
following command will add the system compiler to the `compilers.yaml`
file in `spack/etc/spack/linux`.

```
$ spack compiler add $(spack location -i gcc@4.8.5)
```

Now install a more recent version of `gcc` that will be used as the
default compiler for most future installations.  We have selected
version `13.2.0` for our current iteration (May 20, 2024).

```
$ spack install gcc@13.2.0
```

This will take quite a while. When that finishes, register the new compiler:

```
$ spack compiler add $(spack location -i gcc@13.2.0)
```


Uncomment the new `gcc` compiler in the `packages.yaml` file at
`spack/etc/spack/packages.yaml`.  The first few lines of the file
should now look like:

```
packages:
  all:
    compiler: [gcc@13.2.0]
    variants: ~mpi

```
You should now uncomment any packages in the `packages.yaml` file that
you want to constrain to a particular version, or which you want to
specify to use the system version (`buildable: false`).  The versions
that are currently commented out in your `packages.yaml` that you
copied from the repo should be updated as appropriate (e.g. matching
the versions of any common dependencies that were installed for the
`gcc` compiler), if you decide to set them.  Bennet Fauber initiated
the practice of setting versions/variants for common dependencies when
we were setting up the first U of Mich Spack instance.  This seemed to work
fine, mostly, but we did encounter some issues with having `openssl`
defined.  I believe it was creating a conflict with the system
installed version, which was different.
