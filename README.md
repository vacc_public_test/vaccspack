# Vaccspack

These are files that provide local customization for the Spack
installation that is intended to provide general software packages on
the VACC clusters.  The Spack installation will cohabit software
installed in the traditional manner.  We are consciously restricting
ourselves to non-MPI software.

The presumption is that this documentation and example config files
will be used to set up the test Spack (i.e. sandbox) for a new
contributor, so all the settings in the configuration files presume a
root directory of `$HOME/spk-sandbox`, and that all Spack created
files (including temporary files) will be in directories beneath it.

For example, the path for user `mjohns89sw` would be
`/gpfs1/home/m/j/mjohns89sw/spk-sandbox`.  If you choose a different
root path for your sandbox, be sure to take that into account when
following subsequent instructions.

The files in the repository can be copied and then modified as
described within to create a Spack installation that provides a structure
parallel to the production installation and is intended for real
users.  The targets for the production instance would be

Spack:      `/gpfs1/sw/spack/${SPACK_VER}`<br>
Software:   `/gpfs1/sw/spack/${SPACK_VER}/pkgs`<br>
Modules:    `/gpfs1/sw/spack/${SPACK_VER}/modules`

where ${SPACK_VER} would be replaced with chosen version of the Spack
release.  For example, as of 20 May 2024, we will be using version
0.21.2, and that would be our ${SPACK_VER}. In some places within this
repository, `<ver>` may be used interchangeably.

If and when custom modifications to a Spack package are created, the
`package.py` file would be saved in the following directory:

Local-pkgs: `/gpfs1/sw/spack/${SPACK_VER}/local-packages`.

See `spack-sndbox.md` for detailed instructions on setting up your own
Spack sandbox.  See `spack-setup-prod.md` for detailed instructions on
installing and configuring Spack for production use.